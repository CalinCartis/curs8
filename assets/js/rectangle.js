function switchClassesRectangle() {
  const rectangles = document.getElementsByClassName("rectangle");

  for (i = 0; i < rectangles.length; i++) {
    let rectangle = rectangles[i];
    if (rectangle.classList.contains("bg-success")) {
      rectangle.classList.remove("bg-success");
      rectangle.classList.add("bg-danger");
    } else {
      rectangle.classList.remove("bg-danger");
      rectangle.classList.add("bg-success");
    }
  }
}
