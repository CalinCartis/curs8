function switchClassesCircle() {
  const circles = document.getElementsByClassName("rounded-circle");

  for (i = 0; i < circles.length; i++) {
    let circle = circles[i];
    if (circle.classList.contains("bg-success")) {
      circle.classList.remove("bg-success");
      circle.classList.add("bg-danger");
    } else {
      circle.classList.remove("bg-danger");
      circle.classList.add("bg-success");
    }
  }

  // if (event.target.classList.contains("bg-success")) {
  //   event.target.classList.remove("bg-success");
  //   event.target.classList.add("bg-danger");
  // } else {
  //   event.target.classList.remove("bg-danger");
  //   event.target.classList.add("bg-success");
  // }
}
